find_package( libgclib QUIET)
find_package( ZLIB )

if ( NOT "${LIBGCLIB_FOUND}" )
  message("Disabling libGalil due to missing dependencies")
  message(" libgclib = ${LIBGCLIB_FOUND}")

  set(libGalil_FOUND FALSE PARENT_SCOPE)
  return()
endif()

#
# Prepare the library  
add_library(Galil SHARED)
target_sources(Galil
  PRIVATE
  arrays.cpp
  gclibo.cpp
  )
if(APPLE)
  target_link_libraries(Galil /Applications/gclib/dylib/gclib.0.dylib /Applications/gclib/dylib/gclibo.0.dylib)
else()
  target_link_libraries(Galil gclib gclibo ${ZLIB_LIBRARIES} )
endif()

# Tell rest of labRemote that the library exists
set(libGalil_FOUND TRUE PARENT_SCOPE)
